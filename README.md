# Test

```
docker build -t my_docker_image:latest .
docker images | grep my_docker_image
docker run --name my_container -p 8000:8000 my_docker_image:latest
http://localhost:8000/api/
docker stop my_container
docker rm my_container
```

# TODO

#### add .dockerignore
#### add the config below to settings.py
```
# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/4.1/howto/static-files/
STATIC_ROOT = os.path.join(BASE_DIR, "static")  # <-- static 파일들이 생성되는 곳
STATIC_URL = "static/"  # <-- "/"으로 끝나야 함. client에서 static 파일 접근시 사용되는 path
```

#### add Dockerfile
#### add .gitlab-ci

#### create GCP project

#### enable APIs
- Cloud Build API

#### create a bucket (for log)
name: ${GCP_PROJECT_ID}-log-bucket
k8s-test-lab-416801-log-bucket

#### create key file after creating sa with the roles below (for pushing)
- Cloud Build Service Account
- Service Account User
- Storage Admin

#### base64
```
base64 -i <cicd-keyfile>.json
ewogICJ0eXBlIjogInNlcnZpY2VfYW.....
```

#### gitlab: create var with the base64 above 
1. go to settings > ci/cd > variables
2. create BASE64_GOOGLE_CLOUD_CREDENTIALS

#### gitlab-ci
- set IMAGE_NAME
- build and push
<---- tag가 version, latest 모두 있도록 함. 특히 latest
https://gitlab.com/hojung.yun/my-public-django-k8s-2024/-/pipelines

#### test with image in repo
```
docker run --name my_test_container -p 8000:8000 gcr.io/k8s-test-lab-416801/my-public-django-k8s-2024
```

#### create yaml
- deployment.yaml <--- secret 사용포함
- service.yaml <--- LB type으로 하면 계속 pending. ClusterIP로 해야 함. 그리고 나중에 port forwarding??


#### k8s
- create secret with json key file <--- argocd에서 registry 접근을 위해
```
kubectl create secret docker-registry gcr-secret \
	--docker-server=gcr.io \
	--docker-username=_json_key \
	--docker-password="$(cat sa_key.json)" \
	--docker-email=hojung.yun@gmail.com
kubectl get secret
kubectl delete secret gcr-secret
```

#### test in k8s
```
kubectl get secret
kc apply -f deployment.yaml
```

#### argocd
- create app
- sync

#### test from k8s
curl http://10.110.211.36

#### test from Mac
???

todo: gitlab private 변경 -> argo에서 repo 등록 및 테스트 -> MetalLB 설치/설정 -> LB 서비스 사용 -> proxy 상시 실행 -> auto sync -> db, volume 연결
todo: gitlab private 변경 -> argo에서 repo 등록 및 테스트 -> MetalLB 설치/설정 -> LB 서비스 사용 -> proxy 상시 실행 -> auto sync -> db, volume 연결
todo: gitlab private 변경 -> argo에서 repo 등록 및 테스트 -> MetalLB 설치/설정 -> LB 서비스 사용 -> proxy 상시 실행 -> auto sync -> db, volume 연결
todo: gitlab private 변경 -> argo에서 repo 등록 및 테스트 -> MetalLB 설치/설정 -> LB 서비스 사용 -> proxy 상시 실행 -> auto sync -> db, volume 연결
todo: gitlab private 변경 -> argo에서 repo 등록 및 테스트 -> MetalLB 설치/설정 -> LB 서비스 사용 -> proxy 상시 실행 -> auto sync -> db, volume 연결
