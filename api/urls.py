from django.urls import path
from . import views

urlpatterns = [path("", views.index)]  # <-- 웹요청시 http(s)://host/api/ 경로가 됨
