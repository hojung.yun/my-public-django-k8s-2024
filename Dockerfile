# Use the official Python image as a base
FROM python:3.11-slim

ENV PYTHONUNBUFFERED 1

# NOTE: By default, Ubuntu installs recommended but not suggested packages. With --no-install-recommends,
#       only the main dependencies (packages in the Depends field) are installed.
RUN apt-get update && apt-get install -qq -y --no-install-recommends

# Create application directory (컨테이너 상의 소스코드 디렉토리)
WORKDIR /app

# Copy all except for the files/directories from .dockerignore
COPY . .

# Install dependencies
RUN pip install --upgrade pip
RUN pip install poetry
RUN poetry config virtualenvs.create false
RUN poetry install --no-interaction --no-ansi

# Create static content in <project>/ directory
RUN python manage.py collectstatic --no-input

# Apply DB changes
RUN python manage.py migrate


# Expose port 8000 to the outside world
EXPOSE 8000

# Run the Django application
CMD ["gunicorn", "--bind", "0.0.0.0:8000", "core.wsgi:application"]
